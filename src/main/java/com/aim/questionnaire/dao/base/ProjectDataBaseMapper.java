package com.aim.questionnaire.dao.base;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.aim.questionnaire.dao.entity.ProjectData;
/**
*  @author author
*/
public interface ProjectDataBaseMapper {

    int insertProjectData(ProjectData object);

    int updateProjectData(ProjectData object);

    int update(ProjectData.UpdateBuilder object);

    List<ProjectData> queryProjectData(ProjectData object);

    ProjectData queryProjectDataLimit1(ProjectData object);

}
