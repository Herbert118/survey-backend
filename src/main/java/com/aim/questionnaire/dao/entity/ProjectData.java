package com.aim.questionnaire.dao.entity;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
/**
*
*  @author author
*/
public class ProjectData implements Serializable {

    private static final long serialVersionUID = 1656587413415L;


    /**
    * 主键
    * 项目表主键
    * isNullAble:0
    */
    private String id;

    /**
    * 项目名称
    * isNullAble:1
    */
    private String projectName;

    /**
    * 创建者
    * isNullAble:1
    */
    private String createdBy;

    /**
    * 项目说明
    * isNullAble:1
    */
    private String projectDescription;

    /**
    * 项目状态
    * isNullAble:1
    */
    private String projectStatus;


    public void setId(String id){this.id = id;}

    public String getId(){return this.id;}

    public void setProjectName(String projectName){this.projectName = projectName;}

    public String getProjectName(){return this.projectName;}

    public void setCreatedBy(String createdBy){this.createdBy = createdBy;}

    public String getCreatedBy(){return this.createdBy;}

    public void setProjectDescription(String projectDescription){this.projectDescription = projectDescription;}

    public String getProjectDescription(){return this.projectDescription;}

    public void setProjectStatus(String projectStatus){this.projectStatus = projectStatus;}

    public String getProjectStatus(){return this.projectStatus;}
    @Override
    public String toString() {
        return "ProjectData{" +
                "id='" + id + '\'' +
                "projectName='" + projectName + '\'' +
                "createdBy='" + createdBy + '\'' +
                "projectDescription='" + projectDescription + '\'' +
                "projectStatus='" + projectStatus + '\'' +
            '}';
    }

    public static Builder bBuild(){return new Builder();}

    public static ConditionBuilder ConditionBuild(){return new ConditionBuilder();}

    public static UpdateBuilder UpdateBuild(){return new UpdateBuilder();}

    public static QueryBuilder queryBuild(){return new QueryBuilder();}

    public static class UpdateBuilder {

        private ProjectData set;

        private ConditionBuilder where;

        public UpdateBuilder set(ProjectData set){
            this.set = set;
            return this;
        }

        public ProjectData getSet(){
            return this.set;
        }

        public UpdateBuilder where(ConditionBuilder where){
            this.where = where;
            return this;
        }

        public ConditionBuilder getWhere(){
            return this.where;
        }

        public UpdateBuilder build(){
            return this;
        }
    }

    public static class QueryBuilder extends ProjectData{
        /**
        * 需要返回的列
        */
        private Map<String,Object> fetchFields;

        public Map<String,Object> getFetchFields(){return this.fetchFields;}

        private List<String> idList;

        public List<String> getIdList(){return this.idList;}


        private List<String> fuzzyId;

        public List<String> getFuzzyId(){return this.fuzzyId;}

        private List<String> rightFuzzyId;

        public List<String> getRightFuzzyId(){return this.rightFuzzyId;}
        private List<String> projectNameList;

        public List<String> getProjectNameList(){return this.projectNameList;}


        private List<String> fuzzyProjectName;

        public List<String> getFuzzyProjectName(){return this.fuzzyProjectName;}

        private List<String> rightFuzzyProjectName;

        public List<String> getRightFuzzyProjectName(){return this.rightFuzzyProjectName;}
        private List<String> createdByList;

        public List<String> getCreatedByList(){return this.createdByList;}


        private List<String> fuzzyCreatedBy;

        public List<String> getFuzzyCreatedBy(){return this.fuzzyCreatedBy;}

        private List<String> rightFuzzyCreatedBy;

        public List<String> getRightFuzzyCreatedBy(){return this.rightFuzzyCreatedBy;}
        private List<String> projectDescriptionList;

        public List<String> getProjectDescriptionList(){return this.projectDescriptionList;}


        private List<String> fuzzyProjectDescription;

        public List<String> getFuzzyProjectDescription(){return this.fuzzyProjectDescription;}

        private List<String> rightFuzzyProjectDescription;

        public List<String> getRightFuzzyProjectDescription(){return this.rightFuzzyProjectDescription;}
        private List<String> projectStatusList;

        public List<String> getProjectStatusList(){return this.projectStatusList;}


        private List<String> fuzzyProjectStatus;

        public List<String> getFuzzyProjectStatus(){return this.fuzzyProjectStatus;}

        private List<String> rightFuzzyProjectStatus;

        public List<String> getRightFuzzyProjectStatus(){return this.rightFuzzyProjectStatus;}
        private QueryBuilder (){
            this.fetchFields = new HashMap<>();
        }

        public QueryBuilder fuzzyId (List<String> fuzzyId){
            this.fuzzyId = fuzzyId;
            return this;
        }

        public QueryBuilder fuzzyId (String ... fuzzyId){
            this.fuzzyId = solveNullList(fuzzyId);
            return this;
        }

        public QueryBuilder rightFuzzyId (List<String> rightFuzzyId){
            this.rightFuzzyId = rightFuzzyId;
            return this;
        }

        public QueryBuilder rightFuzzyId (String ... rightFuzzyId){
            this.rightFuzzyId = solveNullList(rightFuzzyId);
            return this;
        }

        public QueryBuilder id(String id){
            setId(id);
            return this;
        }

        public QueryBuilder idList(String ... id){
            this.idList = solveNullList(id);
            return this;
        }

        public QueryBuilder idList(List<String> id){
            this.idList = id;
            return this;
        }

        public QueryBuilder fetchId(){
            setFetchFields("fetchFields","id");
            return this;
        }

        public QueryBuilder excludeId(){
            setFetchFields("excludeFields","id");
            return this;
        }

        public QueryBuilder fuzzyProjectName (List<String> fuzzyProjectName){
            this.fuzzyProjectName = fuzzyProjectName;
            return this;
        }

        public QueryBuilder fuzzyProjectName (String ... fuzzyProjectName){
            this.fuzzyProjectName = solveNullList(fuzzyProjectName);
            return this;
        }

        public QueryBuilder rightFuzzyProjectName (List<String> rightFuzzyProjectName){
            this.rightFuzzyProjectName = rightFuzzyProjectName;
            return this;
        }

        public QueryBuilder rightFuzzyProjectName (String ... rightFuzzyProjectName){
            this.rightFuzzyProjectName = solveNullList(rightFuzzyProjectName);
            return this;
        }

        public QueryBuilder projectName(String projectName){
            setProjectName(projectName);
            return this;
        }

        public QueryBuilder projectNameList(String ... projectName){
            this.projectNameList = solveNullList(projectName);
            return this;
        }

        public QueryBuilder projectNameList(List<String> projectName){
            this.projectNameList = projectName;
            return this;
        }

        public QueryBuilder fetchProjectName(){
            setFetchFields("fetchFields","projectName");
            return this;
        }

        public QueryBuilder excludeProjectName(){
            setFetchFields("excludeFields","projectName");
            return this;
        }

        public QueryBuilder fuzzyCreatedBy (List<String> fuzzyCreatedBy){
            this.fuzzyCreatedBy = fuzzyCreatedBy;
            return this;
        }

        public QueryBuilder fuzzyCreatedBy (String ... fuzzyCreatedBy){
            this.fuzzyCreatedBy = solveNullList(fuzzyCreatedBy);
            return this;
        }

        public QueryBuilder rightFuzzyCreatedBy (List<String> rightFuzzyCreatedBy){
            this.rightFuzzyCreatedBy = rightFuzzyCreatedBy;
            return this;
        }

        public QueryBuilder rightFuzzyCreatedBy (String ... rightFuzzyCreatedBy){
            this.rightFuzzyCreatedBy = solveNullList(rightFuzzyCreatedBy);
            return this;
        }

        public QueryBuilder createdBy(String createdBy){
            setCreatedBy(createdBy);
            return this;
        }

        public QueryBuilder createdByList(String ... createdBy){
            this.createdByList = solveNullList(createdBy);
            return this;
        }

        public QueryBuilder createdByList(List<String> createdBy){
            this.createdByList = createdBy;
            return this;
        }

        public QueryBuilder fetchCreatedBy(){
            setFetchFields("fetchFields","createdBy");
            return this;
        }

        public QueryBuilder excludeCreatedBy(){
            setFetchFields("excludeFields","createdBy");
            return this;
        }

        public QueryBuilder fuzzyProjectDescription (List<String> fuzzyProjectDescription){
            this.fuzzyProjectDescription = fuzzyProjectDescription;
            return this;
        }

        public QueryBuilder fuzzyProjectDescription (String ... fuzzyProjectDescription){
            this.fuzzyProjectDescription = solveNullList(fuzzyProjectDescription);
            return this;
        }

        public QueryBuilder rightFuzzyProjectDescription (List<String> rightFuzzyProjectDescription){
            this.rightFuzzyProjectDescription = rightFuzzyProjectDescription;
            return this;
        }

        public QueryBuilder rightFuzzyProjectDescription (String ... rightFuzzyProjectDescription){
            this.rightFuzzyProjectDescription = solveNullList(rightFuzzyProjectDescription);
            return this;
        }

        public QueryBuilder projectDescription(String projectDescription){
            setProjectDescription(projectDescription);
            return this;
        }

        public QueryBuilder projectDescriptionList(String ... projectDescription){
            this.projectDescriptionList = solveNullList(projectDescription);
            return this;
        }

        public QueryBuilder projectDescriptionList(List<String> projectDescription){
            this.projectDescriptionList = projectDescription;
            return this;
        }

        public QueryBuilder fetchProjectDescription(){
            setFetchFields("fetchFields","projectDescription");
            return this;
        }

        public QueryBuilder excludeProjectDescription(){
            setFetchFields("excludeFields","projectDescription");
            return this;
        }

        public QueryBuilder fuzzyProjectStatus (List<String> fuzzyProjectStatus){
            this.fuzzyProjectStatus = fuzzyProjectStatus;
            return this;
        }

        public QueryBuilder fuzzyProjectStatus (String ... fuzzyProjectStatus){
            this.fuzzyProjectStatus = solveNullList(fuzzyProjectStatus);
            return this;
        }

        public QueryBuilder rightFuzzyProjectStatus (List<String> rightFuzzyProjectStatus){
            this.rightFuzzyProjectStatus = rightFuzzyProjectStatus;
            return this;
        }

        public QueryBuilder rightFuzzyProjectStatus (String ... rightFuzzyProjectStatus){
            this.rightFuzzyProjectStatus = solveNullList(rightFuzzyProjectStatus);
            return this;
        }

        public QueryBuilder projectStatus(String projectStatus){
            setProjectStatus(projectStatus);
            return this;
        }

        public QueryBuilder projectStatusList(String ... projectStatus){
            this.projectStatusList = solveNullList(projectStatus);
            return this;
        }

        public QueryBuilder projectStatusList(List<String> projectStatus){
            this.projectStatusList = projectStatus;
            return this;
        }

        public QueryBuilder fetchProjectStatus(){
            setFetchFields("fetchFields","projectStatus");
            return this;
        }

        public QueryBuilder excludeProjectStatus(){
            setFetchFields("excludeFields","projectStatus");
            return this;
        }
        private <T>List<T> solveNullList(T ... objs){
            if (objs != null){
            List<T> list = new ArrayList<>();
                for (T item : objs){
                    if (item != null){
                        list.add(item);
                    }
                }
                return list;
            }
            return null;
        }

        public QueryBuilder fetchAll(){
            this.fetchFields.put("AllFields",true);
            return this;
        }

        public QueryBuilder addField(String ... fields){
            List<String> list = new ArrayList<>();
            if (fields != null){
                for (String field : fields){
                    list.add(field);
                }
            }
            this.fetchFields.put("otherFields",list);
            return this;
        }
        @SuppressWarnings("unchecked")
        private void setFetchFields(String key,String val){
            Map<String,Boolean> fields= (Map<String, Boolean>) this.fetchFields.get(key);
            if (fields == null){
                fields = new HashMap<>();
            }
            fields.put(val,true);
            this.fetchFields.put(key,fields);
        }

        public ProjectData build(){return this;}
    }


    public static class ConditionBuilder{
        private List<String> idList;

        public List<String> getIdList(){return this.idList;}


        private List<String> fuzzyId;

        public List<String> getFuzzyId(){return this.fuzzyId;}

        private List<String> rightFuzzyId;

        public List<String> getRightFuzzyId(){return this.rightFuzzyId;}
        private List<String> projectNameList;

        public List<String> getProjectNameList(){return this.projectNameList;}


        private List<String> fuzzyProjectName;

        public List<String> getFuzzyProjectName(){return this.fuzzyProjectName;}

        private List<String> rightFuzzyProjectName;

        public List<String> getRightFuzzyProjectName(){return this.rightFuzzyProjectName;}
        private List<String> createdByList;

        public List<String> getCreatedByList(){return this.createdByList;}


        private List<String> fuzzyCreatedBy;

        public List<String> getFuzzyCreatedBy(){return this.fuzzyCreatedBy;}

        private List<String> rightFuzzyCreatedBy;

        public List<String> getRightFuzzyCreatedBy(){return this.rightFuzzyCreatedBy;}
        private List<String> projectDescriptionList;

        public List<String> getProjectDescriptionList(){return this.projectDescriptionList;}


        private List<String> fuzzyProjectDescription;

        public List<String> getFuzzyProjectDescription(){return this.fuzzyProjectDescription;}

        private List<String> rightFuzzyProjectDescription;

        public List<String> getRightFuzzyProjectDescription(){return this.rightFuzzyProjectDescription;}
        private List<String> projectStatusList;

        public List<String> getProjectStatusList(){return this.projectStatusList;}


        private List<String> fuzzyProjectStatus;

        public List<String> getFuzzyProjectStatus(){return this.fuzzyProjectStatus;}

        private List<String> rightFuzzyProjectStatus;

        public List<String> getRightFuzzyProjectStatus(){return this.rightFuzzyProjectStatus;}

        public ConditionBuilder fuzzyId (List<String> fuzzyId){
            this.fuzzyId = fuzzyId;
            return this;
        }

        public ConditionBuilder fuzzyId (String ... fuzzyId){
            this.fuzzyId = solveNullList(fuzzyId);
            return this;
        }

        public ConditionBuilder rightFuzzyId (List<String> rightFuzzyId){
            this.rightFuzzyId = rightFuzzyId;
            return this;
        }

        public ConditionBuilder rightFuzzyId (String ... rightFuzzyId){
            this.rightFuzzyId = solveNullList(rightFuzzyId);
            return this;
        }

        public ConditionBuilder idList(String ... id){
            this.idList = solveNullList(id);
            return this;
        }

        public ConditionBuilder idList(List<String> id){
            this.idList = id;
            return this;
        }

        public ConditionBuilder fuzzyProjectName (List<String> fuzzyProjectName){
            this.fuzzyProjectName = fuzzyProjectName;
            return this;
        }

        public ConditionBuilder fuzzyProjectName (String ... fuzzyProjectName){
            this.fuzzyProjectName = solveNullList(fuzzyProjectName);
            return this;
        }

        public ConditionBuilder rightFuzzyProjectName (List<String> rightFuzzyProjectName){
            this.rightFuzzyProjectName = rightFuzzyProjectName;
            return this;
        }

        public ConditionBuilder rightFuzzyProjectName (String ... rightFuzzyProjectName){
            this.rightFuzzyProjectName = solveNullList(rightFuzzyProjectName);
            return this;
        }

        public ConditionBuilder projectNameList(String ... projectName){
            this.projectNameList = solveNullList(projectName);
            return this;
        }

        public ConditionBuilder projectNameList(List<String> projectName){
            this.projectNameList = projectName;
            return this;
        }

        public ConditionBuilder fuzzyCreatedBy (List<String> fuzzyCreatedBy){
            this.fuzzyCreatedBy = fuzzyCreatedBy;
            return this;
        }

        public ConditionBuilder fuzzyCreatedBy (String ... fuzzyCreatedBy){
            this.fuzzyCreatedBy = solveNullList(fuzzyCreatedBy);
            return this;
        }

        public ConditionBuilder rightFuzzyCreatedBy (List<String> rightFuzzyCreatedBy){
            this.rightFuzzyCreatedBy = rightFuzzyCreatedBy;
            return this;
        }

        public ConditionBuilder rightFuzzyCreatedBy (String ... rightFuzzyCreatedBy){
            this.rightFuzzyCreatedBy = solveNullList(rightFuzzyCreatedBy);
            return this;
        }

        public ConditionBuilder createdByList(String ... createdBy){
            this.createdByList = solveNullList(createdBy);
            return this;
        }

        public ConditionBuilder createdByList(List<String> createdBy){
            this.createdByList = createdBy;
            return this;
        }

        public ConditionBuilder fuzzyProjectDescription (List<String> fuzzyProjectDescription){
            this.fuzzyProjectDescription = fuzzyProjectDescription;
            return this;
        }

        public ConditionBuilder fuzzyProjectDescription (String ... fuzzyProjectDescription){
            this.fuzzyProjectDescription = solveNullList(fuzzyProjectDescription);
            return this;
        }

        public ConditionBuilder rightFuzzyProjectDescription (List<String> rightFuzzyProjectDescription){
            this.rightFuzzyProjectDescription = rightFuzzyProjectDescription;
            return this;
        }

        public ConditionBuilder rightFuzzyProjectDescription (String ... rightFuzzyProjectDescription){
            this.rightFuzzyProjectDescription = solveNullList(rightFuzzyProjectDescription);
            return this;
        }

        public ConditionBuilder projectDescriptionList(String ... projectDescription){
            this.projectDescriptionList = solveNullList(projectDescription);
            return this;
        }

        public ConditionBuilder projectDescriptionList(List<String> projectDescription){
            this.projectDescriptionList = projectDescription;
            return this;
        }

        public ConditionBuilder fuzzyProjectStatus (List<String> fuzzyProjectStatus){
            this.fuzzyProjectStatus = fuzzyProjectStatus;
            return this;
        }

        public ConditionBuilder fuzzyProjectStatus (String ... fuzzyProjectStatus){
            this.fuzzyProjectStatus = solveNullList(fuzzyProjectStatus);
            return this;
        }

        public ConditionBuilder rightFuzzyProjectStatus (List<String> rightFuzzyProjectStatus){
            this.rightFuzzyProjectStatus = rightFuzzyProjectStatus;
            return this;
        }

        public ConditionBuilder rightFuzzyProjectStatus (String ... rightFuzzyProjectStatus){
            this.rightFuzzyProjectStatus = solveNullList(rightFuzzyProjectStatus);
            return this;
        }

        public ConditionBuilder projectStatusList(String ... projectStatus){
            this.projectStatusList = solveNullList(projectStatus);
            return this;
        }

        public ConditionBuilder projectStatusList(List<String> projectStatus){
            this.projectStatusList = projectStatus;
            return this;
        }

        private <T>List<T> solveNullList(T ... objs){
            if (objs != null){
            List<T> list = new ArrayList<>();
                for (T item : objs){
                    if (item != null){
                        list.add(item);
                    }
                }
                return list;
            }
            return null;
        }

        public ConditionBuilder build(){return this;}
    }

    public static class Builder {

        private ProjectData obj;

        public Builder(){
            this.obj = new ProjectData();
        }

        public Builder id(String id){
            this.obj.setId(id);
            return this;
        }
        public Builder projectName(String projectName){
            this.obj.setProjectName(projectName);
            return this;
        }
        public Builder createdBy(String createdBy){
            this.obj.setCreatedBy(createdBy);
            return this;
        }
        public Builder projectDescription(String projectDescription){
            this.obj.setProjectDescription(projectDescription);
            return this;
        }
        public Builder projectStatus(String projectStatus){
            this.obj.setProjectStatus(projectStatus);
            return this;
        }
        public ProjectData build(){return obj;}
    }

}
