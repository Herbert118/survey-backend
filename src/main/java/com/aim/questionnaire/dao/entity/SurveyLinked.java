package com.aim.questionnaire.dao.entity;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
/**
*
*  @author author
*/
public class SurveyLinked implements Serializable {

    private static final long serialVersionUID = 1656467318079L;


    /**
    * 主键
    * 问卷模板表主键
    * isNullAble:0
    */
    private String id;

    /**
    * 问卷名称
    * isNullAble:1
    */
    private String surveyName;

    /**
    * 问卷内容说明
    * isNullAble:1
    */
    private String surveyContent;

    /**
    * 问卷状态
    * isNullAble:1
    */
    private Integer surveyStatus;

    /**
    * 问卷关联项目id
    * isNullAble:0
    */
    private String projectId;

    /**
    * 开始时间
    * isNullAble:1
    */
    private java.time.LocalDateTime startTime;

    /**
    * 结束时间
    * isNullAble:1
    */
    private java.time.LocalDateTime endTime;


    public void setId(String id){this.id = id;}

    public String getId(){return this.id;}

    public void setSurveyName(String surveyName){this.surveyName = surveyName;}

    public String getSurveyName(){return this.surveyName;}

    public void setSurveyContent(String surveyContent){this.surveyContent = surveyContent;}

    public String getSurveyContent(){return this.surveyContent;}

    public void setSurveyStatus(Integer surveyStatus){this.surveyStatus = surveyStatus;}

    public Integer getSurveyStatus(){return this.surveyStatus;}

    public void setProjectId(String projectId){this.projectId = projectId;}

    public String getProjectId(){return this.projectId;}

    public void setStartTime(java.time.LocalDateTime startTime){this.startTime = startTime;}

    public java.time.LocalDateTime getStartTime(){return this.startTime;}

    public void setEndTime(java.time.LocalDateTime endTime){this.endTime = endTime;}

    public java.time.LocalDateTime getEndTime(){return this.endTime;}
    @Override
    public String toString() {
        return "SurveyLinked{" +
                "id='" + id + '\'' +
                "surveyName='" + surveyName + '\'' +
                "surveyContent='" + surveyContent + '\'' +
                "surveyStatus='" + surveyStatus + '\'' +
                "projectId='" + projectId + '\'' +
                "startTime='" + startTime + '\'' +
                "endTime='" + endTime + '\'' +
            '}';
    }

    public static Builder bBuild(){return new Builder();}

    public static ConditionBuilder ConditionBuild(){return new ConditionBuilder();}

    public static UpdateBuilder UpdateBuild(){return new UpdateBuilder();}

    public static QueryBuilder queryBuild(){return new QueryBuilder();}

    public static class UpdateBuilder {

        private SurveyLinked set;

        private ConditionBuilder where;

        public UpdateBuilder set(SurveyLinked set){
            this.set = set;
            return this;
        }

        public SurveyLinked getSet(){
            return this.set;
        }

        public UpdateBuilder where(ConditionBuilder where){
            this.where = where;
            return this;
        }

        public ConditionBuilder getWhere(){
            return this.where;
        }

        public UpdateBuilder build(){
            return this;
        }
    }

    public static class QueryBuilder extends SurveyLinked{
        /**
        * 需要返回的列
        */
        private Map<String,Object> fetchFields;

        public Map<String,Object> getFetchFields(){return this.fetchFields;}

        private List<String> idList;

        public List<String> getIdList(){return this.idList;}


        private List<String> fuzzyId;

        public List<String> getFuzzyId(){return this.fuzzyId;}

        private List<String> rightFuzzyId;

        public List<String> getRightFuzzyId(){return this.rightFuzzyId;}
        private List<String> surveyNameList;

        public List<String> getSurveyNameList(){return this.surveyNameList;}


        private List<String> fuzzySurveyName;

        public List<String> getFuzzySurveyName(){return this.fuzzySurveyName;}

        private List<String> rightFuzzySurveyName;

        public List<String> getRightFuzzySurveyName(){return this.rightFuzzySurveyName;}
        private List<String> surveyContentList;

        public List<String> getSurveyContentList(){return this.surveyContentList;}


        private List<String> fuzzySurveyContent;

        public List<String> getFuzzySurveyContent(){return this.fuzzySurveyContent;}

        private List<String> rightFuzzySurveyContent;

        public List<String> getRightFuzzySurveyContent(){return this.rightFuzzySurveyContent;}
        private List<Integer> surveyStatusList;

        public List<Integer> getSurveyStatusList(){return this.surveyStatusList;}

        private Integer surveyStatusSt;

        private Integer surveyStatusEd;

        public Integer getSurveyStatusSt(){return this.surveyStatusSt;}

        public Integer getSurveyStatusEd(){return this.surveyStatusEd;}

        private List<String> projectIdList;

        public List<String> getProjectIdList(){return this.projectIdList;}


        private List<String> fuzzyProjectId;

        public List<String> getFuzzyProjectId(){return this.fuzzyProjectId;}

        private List<String> rightFuzzyProjectId;

        public List<String> getRightFuzzyProjectId(){return this.rightFuzzyProjectId;}
        private List<java.time.LocalDateTime> startTimeList;

        public List<java.time.LocalDateTime> getStartTimeList(){return this.startTimeList;}

        private java.time.LocalDateTime startTimeSt;

        private java.time.LocalDateTime startTimeEd;

        public java.time.LocalDateTime getStartTimeSt(){return this.startTimeSt;}

        public java.time.LocalDateTime getStartTimeEd(){return this.startTimeEd;}

        private List<java.time.LocalDateTime> endTimeList;

        public List<java.time.LocalDateTime> getEndTimeList(){return this.endTimeList;}

        private java.time.LocalDateTime endTimeSt;

        private java.time.LocalDateTime endTimeEd;

        public java.time.LocalDateTime getEndTimeSt(){return this.endTimeSt;}

        public java.time.LocalDateTime getEndTimeEd(){return this.endTimeEd;}

        private QueryBuilder (){
            this.fetchFields = new HashMap<>();
        }

        public QueryBuilder fuzzyId (List<String> fuzzyId){
            this.fuzzyId = fuzzyId;
            return this;
        }

        public QueryBuilder fuzzyId (String ... fuzzyId){
            this.fuzzyId = solveNullList(fuzzyId);
            return this;
        }

        public QueryBuilder rightFuzzyId (List<String> rightFuzzyId){
            this.rightFuzzyId = rightFuzzyId;
            return this;
        }

        public QueryBuilder rightFuzzyId (String ... rightFuzzyId){
            this.rightFuzzyId = solveNullList(rightFuzzyId);
            return this;
        }

        public QueryBuilder id(String id){
            setId(id);
            return this;
        }

        public QueryBuilder idList(String ... id){
            this.idList = solveNullList(id);
            return this;
        }

        public QueryBuilder idList(List<String> id){
            this.idList = id;
            return this;
        }

        public QueryBuilder fetchId(){
            setFetchFields("fetchFields","id");
            return this;
        }

        public QueryBuilder excludeId(){
            setFetchFields("excludeFields","id");
            return this;
        }

        public QueryBuilder fuzzySurveyName (List<String> fuzzySurveyName){
            this.fuzzySurveyName = fuzzySurveyName;
            return this;
        }

        public QueryBuilder fuzzySurveyName (String ... fuzzySurveyName){
            this.fuzzySurveyName = solveNullList(fuzzySurveyName);
            return this;
        }

        public QueryBuilder rightFuzzySurveyName (List<String> rightFuzzySurveyName){
            this.rightFuzzySurveyName = rightFuzzySurveyName;
            return this;
        }

        public QueryBuilder rightFuzzySurveyName (String ... rightFuzzySurveyName){
            this.rightFuzzySurveyName = solveNullList(rightFuzzySurveyName);
            return this;
        }

        public QueryBuilder surveyName(String surveyName){
            setSurveyName(surveyName);
            return this;
        }

        public QueryBuilder surveyNameList(String ... surveyName){
            this.surveyNameList = solveNullList(surveyName);
            return this;
        }

        public QueryBuilder surveyNameList(List<String> surveyName){
            this.surveyNameList = surveyName;
            return this;
        }

        public QueryBuilder fetchSurveyName(){
            setFetchFields("fetchFields","surveyName");
            return this;
        }

        public QueryBuilder excludeSurveyName(){
            setFetchFields("excludeFields","surveyName");
            return this;
        }

        public QueryBuilder fuzzySurveyContent (List<String> fuzzySurveyContent){
            this.fuzzySurveyContent = fuzzySurveyContent;
            return this;
        }

        public QueryBuilder fuzzySurveyContent (String ... fuzzySurveyContent){
            this.fuzzySurveyContent = solveNullList(fuzzySurveyContent);
            return this;
        }

        public QueryBuilder rightFuzzySurveyContent (List<String> rightFuzzySurveyContent){
            this.rightFuzzySurveyContent = rightFuzzySurveyContent;
            return this;
        }

        public QueryBuilder rightFuzzySurveyContent (String ... rightFuzzySurveyContent){
            this.rightFuzzySurveyContent = solveNullList(rightFuzzySurveyContent);
            return this;
        }

        public QueryBuilder surveyContent(String surveyContent){
            setSurveyContent(surveyContent);
            return this;
        }

        public QueryBuilder surveyContentList(String ... surveyContent){
            this.surveyContentList = solveNullList(surveyContent);
            return this;
        }

        public QueryBuilder surveyContentList(List<String> surveyContent){
            this.surveyContentList = surveyContent;
            return this;
        }

        public QueryBuilder fetchSurveyContent(){
            setFetchFields("fetchFields","surveyContent");
            return this;
        }

        public QueryBuilder excludeSurveyContent(){
            setFetchFields("excludeFields","surveyContent");
            return this;
        }

        public QueryBuilder surveyStatusBetWeen(Integer surveyStatusSt,Integer surveyStatusEd){
            this.surveyStatusSt = surveyStatusSt;
            this.surveyStatusEd = surveyStatusEd;
            return this;
        }

        public QueryBuilder surveyStatusGreaterEqThan(Integer surveyStatusSt){
            this.surveyStatusSt = surveyStatusSt;
            return this;
        }
        public QueryBuilder surveyStatusLessEqThan(Integer surveyStatusEd){
            this.surveyStatusEd = surveyStatusEd;
            return this;
        }


        public QueryBuilder surveyStatus(Integer surveyStatus){
            setSurveyStatus(surveyStatus);
            return this;
        }

        public QueryBuilder surveyStatusList(Integer ... surveyStatus){
            this.surveyStatusList = solveNullList(surveyStatus);
            return this;
        }

        public QueryBuilder surveyStatusList(List<Integer> surveyStatus){
            this.surveyStatusList = surveyStatus;
            return this;
        }

        public QueryBuilder fetchSurveyStatus(){
            setFetchFields("fetchFields","surveyStatus");
            return this;
        }

        public QueryBuilder excludeSurveyStatus(){
            setFetchFields("excludeFields","surveyStatus");
            return this;
        }

        public QueryBuilder fuzzyProjectId (List<String> fuzzyProjectId){
            this.fuzzyProjectId = fuzzyProjectId;
            return this;
        }

        public QueryBuilder fuzzyProjectId (String ... fuzzyProjectId){
            this.fuzzyProjectId = solveNullList(fuzzyProjectId);
            return this;
        }

        public QueryBuilder rightFuzzyProjectId (List<String> rightFuzzyProjectId){
            this.rightFuzzyProjectId = rightFuzzyProjectId;
            return this;
        }

        public QueryBuilder rightFuzzyProjectId (String ... rightFuzzyProjectId){
            this.rightFuzzyProjectId = solveNullList(rightFuzzyProjectId);
            return this;
        }

        public QueryBuilder projectId(String projectId){
            setProjectId(projectId);
            return this;
        }

        public QueryBuilder projectIdList(String ... projectId){
            this.projectIdList = solveNullList(projectId);
            return this;
        }

        public QueryBuilder projectIdList(List<String> projectId){
            this.projectIdList = projectId;
            return this;
        }

        public QueryBuilder fetchProjectId(){
            setFetchFields("fetchFields","projectId");
            return this;
        }

        public QueryBuilder excludeProjectId(){
            setFetchFields("excludeFields","projectId");
            return this;
        }

        public QueryBuilder startTimeBetWeen(java.time.LocalDateTime startTimeSt,java.time.LocalDateTime startTimeEd){
            this.startTimeSt = startTimeSt;
            this.startTimeEd = startTimeEd;
            return this;
        }

        public QueryBuilder startTimeGreaterEqThan(java.time.LocalDateTime startTimeSt){
            this.startTimeSt = startTimeSt;
            return this;
        }
        public QueryBuilder startTimeLessEqThan(java.time.LocalDateTime startTimeEd){
            this.startTimeEd = startTimeEd;
            return this;
        }


        public QueryBuilder startTime(java.time.LocalDateTime startTime){
            setStartTime(startTime);
            return this;
        }

        public QueryBuilder startTimeList(java.time.LocalDateTime ... startTime){
            this.startTimeList = solveNullList(startTime);
            return this;
        }

        public QueryBuilder startTimeList(List<java.time.LocalDateTime> startTime){
            this.startTimeList = startTime;
            return this;
        }

        public QueryBuilder fetchStartTime(){
            setFetchFields("fetchFields","startTime");
            return this;
        }

        public QueryBuilder excludeStartTime(){
            setFetchFields("excludeFields","startTime");
            return this;
        }

        public QueryBuilder endTimeBetWeen(java.time.LocalDateTime endTimeSt,java.time.LocalDateTime endTimeEd){
            this.endTimeSt = endTimeSt;
            this.endTimeEd = endTimeEd;
            return this;
        }

        public QueryBuilder endTimeGreaterEqThan(java.time.LocalDateTime endTimeSt){
            this.endTimeSt = endTimeSt;
            return this;
        }
        public QueryBuilder endTimeLessEqThan(java.time.LocalDateTime endTimeEd){
            this.endTimeEd = endTimeEd;
            return this;
        }


        public QueryBuilder endTime(java.time.LocalDateTime endTime){
            setEndTime(endTime);
            return this;
        }

        public QueryBuilder endTimeList(java.time.LocalDateTime ... endTime){
            this.endTimeList = solveNullList(endTime);
            return this;
        }

        public QueryBuilder endTimeList(List<java.time.LocalDateTime> endTime){
            this.endTimeList = endTime;
            return this;
        }

        public QueryBuilder fetchEndTime(){
            setFetchFields("fetchFields","endTime");
            return this;
        }

        public QueryBuilder excludeEndTime(){
            setFetchFields("excludeFields","endTime");
            return this;
        }
        private <T>List<T> solveNullList(T ... objs){
            if (objs != null){
            List<T> list = new ArrayList<>();
                for (T item : objs){
                    if (item != null){
                        list.add(item);
                    }
                }
                return list;
            }
            return null;
        }

        public QueryBuilder fetchAll(){
            this.fetchFields.put("AllFields",true);
            return this;
        }

        public QueryBuilder addField(String ... fields){
            List<String> list = new ArrayList<>();
            if (fields != null){
                for (String field : fields){
                    list.add(field);
                }
            }
            this.fetchFields.put("otherFields",list);
            return this;
        }
        @SuppressWarnings("unchecked")
        private void setFetchFields(String key,String val){
            Map<String,Boolean> fields= (Map<String, Boolean>) this.fetchFields.get(key);
            if (fields == null){
                fields = new HashMap<>();
            }
            fields.put(val,true);
            this.fetchFields.put(key,fields);
        }

        public SurveyLinked build(){return this;}
    }


    public static class ConditionBuilder{
        private List<String> idList;

        public List<String> getIdList(){return this.idList;}


        private List<String> fuzzyId;

        public List<String> getFuzzyId(){return this.fuzzyId;}

        private List<String> rightFuzzyId;

        public List<String> getRightFuzzyId(){return this.rightFuzzyId;}
        private List<String> surveyNameList;

        public List<String> getSurveyNameList(){return this.surveyNameList;}


        private List<String> fuzzySurveyName;

        public List<String> getFuzzySurveyName(){return this.fuzzySurveyName;}

        private List<String> rightFuzzySurveyName;

        public List<String> getRightFuzzySurveyName(){return this.rightFuzzySurveyName;}
        private List<String> surveyContentList;

        public List<String> getSurveyContentList(){return this.surveyContentList;}


        private List<String> fuzzySurveyContent;

        public List<String> getFuzzySurveyContent(){return this.fuzzySurveyContent;}

        private List<String> rightFuzzySurveyContent;

        public List<String> getRightFuzzySurveyContent(){return this.rightFuzzySurveyContent;}
        private List<Integer> surveyStatusList;

        public List<Integer> getSurveyStatusList(){return this.surveyStatusList;}

        private Integer surveyStatusSt;

        private Integer surveyStatusEd;

        public Integer getSurveyStatusSt(){return this.surveyStatusSt;}

        public Integer getSurveyStatusEd(){return this.surveyStatusEd;}

        private List<String> projectIdList;

        public List<String> getProjectIdList(){return this.projectIdList;}


        private List<String> fuzzyProjectId;

        public List<String> getFuzzyProjectId(){return this.fuzzyProjectId;}

        private List<String> rightFuzzyProjectId;

        public List<String> getRightFuzzyProjectId(){return this.rightFuzzyProjectId;}
        private List<java.time.LocalDateTime> startTimeList;

        public List<java.time.LocalDateTime> getStartTimeList(){return this.startTimeList;}

        private java.time.LocalDateTime startTimeSt;

        private java.time.LocalDateTime startTimeEd;

        public java.time.LocalDateTime getStartTimeSt(){return this.startTimeSt;}

        public java.time.LocalDateTime getStartTimeEd(){return this.startTimeEd;}

        private List<java.time.LocalDateTime> endTimeList;

        public List<java.time.LocalDateTime> getEndTimeList(){return this.endTimeList;}

        private java.time.LocalDateTime endTimeSt;

        private java.time.LocalDateTime endTimeEd;

        public java.time.LocalDateTime getEndTimeSt(){return this.endTimeSt;}

        public java.time.LocalDateTime getEndTimeEd(){return this.endTimeEd;}


        public ConditionBuilder fuzzyId (List<String> fuzzyId){
            this.fuzzyId = fuzzyId;
            return this;
        }

        public ConditionBuilder fuzzyId (String ... fuzzyId){
            this.fuzzyId = solveNullList(fuzzyId);
            return this;
        }

        public ConditionBuilder rightFuzzyId (List<String> rightFuzzyId){
            this.rightFuzzyId = rightFuzzyId;
            return this;
        }

        public ConditionBuilder rightFuzzyId (String ... rightFuzzyId){
            this.rightFuzzyId = solveNullList(rightFuzzyId);
            return this;
        }

        public ConditionBuilder idList(String ... id){
            this.idList = solveNullList(id);
            return this;
        }

        public ConditionBuilder idList(List<String> id){
            this.idList = id;
            return this;
        }

        public ConditionBuilder fuzzySurveyName (List<String> fuzzySurveyName){
            this.fuzzySurveyName = fuzzySurveyName;
            return this;
        }

        public ConditionBuilder fuzzySurveyName (String ... fuzzySurveyName){
            this.fuzzySurveyName = solveNullList(fuzzySurveyName);
            return this;
        }

        public ConditionBuilder rightFuzzySurveyName (List<String> rightFuzzySurveyName){
            this.rightFuzzySurveyName = rightFuzzySurveyName;
            return this;
        }

        public ConditionBuilder rightFuzzySurveyName (String ... rightFuzzySurveyName){
            this.rightFuzzySurveyName = solveNullList(rightFuzzySurveyName);
            return this;
        }

        public ConditionBuilder surveyNameList(String ... surveyName){
            this.surveyNameList = solveNullList(surveyName);
            return this;
        }

        public ConditionBuilder surveyNameList(List<String> surveyName){
            this.surveyNameList = surveyName;
            return this;
        }

        public ConditionBuilder fuzzySurveyContent (List<String> fuzzySurveyContent){
            this.fuzzySurveyContent = fuzzySurveyContent;
            return this;
        }

        public ConditionBuilder fuzzySurveyContent (String ... fuzzySurveyContent){
            this.fuzzySurveyContent = solveNullList(fuzzySurveyContent);
            return this;
        }

        public ConditionBuilder rightFuzzySurveyContent (List<String> rightFuzzySurveyContent){
            this.rightFuzzySurveyContent = rightFuzzySurveyContent;
            return this;
        }

        public ConditionBuilder rightFuzzySurveyContent (String ... rightFuzzySurveyContent){
            this.rightFuzzySurveyContent = solveNullList(rightFuzzySurveyContent);
            return this;
        }

        public ConditionBuilder surveyContentList(String ... surveyContent){
            this.surveyContentList = solveNullList(surveyContent);
            return this;
        }

        public ConditionBuilder surveyContentList(List<String> surveyContent){
            this.surveyContentList = surveyContent;
            return this;
        }

        public ConditionBuilder surveyStatusBetWeen(Integer surveyStatusSt,Integer surveyStatusEd){
            this.surveyStatusSt = surveyStatusSt;
            this.surveyStatusEd = surveyStatusEd;
            return this;
        }

        public ConditionBuilder surveyStatusGreaterEqThan(Integer surveyStatusSt){
            this.surveyStatusSt = surveyStatusSt;
            return this;
        }
        public ConditionBuilder surveyStatusLessEqThan(Integer surveyStatusEd){
            this.surveyStatusEd = surveyStatusEd;
            return this;
        }


        public ConditionBuilder surveyStatusList(Integer ... surveyStatus){
            this.surveyStatusList = solveNullList(surveyStatus);
            return this;
        }

        public ConditionBuilder surveyStatusList(List<Integer> surveyStatus){
            this.surveyStatusList = surveyStatus;
            return this;
        }

        public ConditionBuilder fuzzyProjectId (List<String> fuzzyProjectId){
            this.fuzzyProjectId = fuzzyProjectId;
            return this;
        }

        public ConditionBuilder fuzzyProjectId (String ... fuzzyProjectId){
            this.fuzzyProjectId = solveNullList(fuzzyProjectId);
            return this;
        }

        public ConditionBuilder rightFuzzyProjectId (List<String> rightFuzzyProjectId){
            this.rightFuzzyProjectId = rightFuzzyProjectId;
            return this;
        }

        public ConditionBuilder rightFuzzyProjectId (String ... rightFuzzyProjectId){
            this.rightFuzzyProjectId = solveNullList(rightFuzzyProjectId);
            return this;
        }

        public ConditionBuilder projectIdList(String ... projectId){
            this.projectIdList = solveNullList(projectId);
            return this;
        }

        public ConditionBuilder projectIdList(List<String> projectId){
            this.projectIdList = projectId;
            return this;
        }

        public ConditionBuilder startTimeBetWeen(java.time.LocalDateTime startTimeSt,java.time.LocalDateTime startTimeEd){
            this.startTimeSt = startTimeSt;
            this.startTimeEd = startTimeEd;
            return this;
        }

        public ConditionBuilder startTimeGreaterEqThan(java.time.LocalDateTime startTimeSt){
            this.startTimeSt = startTimeSt;
            return this;
        }
        public ConditionBuilder startTimeLessEqThan(java.time.LocalDateTime startTimeEd){
            this.startTimeEd = startTimeEd;
            return this;
        }


        public ConditionBuilder startTimeList(java.time.LocalDateTime ... startTime){
            this.startTimeList = solveNullList(startTime);
            return this;
        }

        public ConditionBuilder startTimeList(List<java.time.LocalDateTime> startTime){
            this.startTimeList = startTime;
            return this;
        }

        public ConditionBuilder endTimeBetWeen(java.time.LocalDateTime endTimeSt,java.time.LocalDateTime endTimeEd){
            this.endTimeSt = endTimeSt;
            this.endTimeEd = endTimeEd;
            return this;
        }

        public ConditionBuilder endTimeGreaterEqThan(java.time.LocalDateTime endTimeSt){
            this.endTimeSt = endTimeSt;
            return this;
        }
        public ConditionBuilder endTimeLessEqThan(java.time.LocalDateTime endTimeEd){
            this.endTimeEd = endTimeEd;
            return this;
        }


        public ConditionBuilder endTimeList(java.time.LocalDateTime ... endTime){
            this.endTimeList = solveNullList(endTime);
            return this;
        }

        public ConditionBuilder endTimeList(List<java.time.LocalDateTime> endTime){
            this.endTimeList = endTime;
            return this;
        }

        private <T>List<T> solveNullList(T ... objs){
            if (objs != null){
            List<T> list = new ArrayList<>();
                for (T item : objs){
                    if (item != null){
                        list.add(item);
                    }
                }
                return list;
            }
            return null;
        }

        public ConditionBuilder build(){return this;}
    }

    public static class Builder {

        private SurveyLinked obj;

        public Builder(){
            this.obj = new SurveyLinked();
        }

        public Builder id(String id){
            this.obj.setId(id);
            return this;
        }
        public Builder surveyName(String surveyName){
            this.obj.setSurveyName(surveyName);
            return this;
        }
        public Builder surveyContent(String surveyContent){
            this.obj.setSurveyContent(surveyContent);
            return this;
        }
        public Builder surveyStatus(Integer surveyStatus){
            this.obj.setSurveyStatus(surveyStatus);
            return this;
        }
        public Builder projectId(String projectId){
            this.obj.setProjectId(projectId);
            return this;
        }
        public Builder startTime(java.time.LocalDateTime startTime){
            this.obj.setStartTime(startTime);
            return this;
        }
        public Builder endTime(java.time.LocalDateTime endTime){
            this.obj.setEndTime(endTime);
            return this;
        }
        public SurveyLinked build(){return obj;}
    }

}
