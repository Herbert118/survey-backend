package com.aim.questionnaire.service;

import com.aim.questionnaire.common.utils.UUIDUtil;
import com.aim.questionnaire.dao.ProjectDataMapper;
import com.aim.questionnaire.dao.SurveyLinkedMapper;
import com.aim.questionnaire.dao.SurveyTemplateMapper;
import com.aim.questionnaire.dao.entity.ProjectData;
import com.aim.questionnaire.dao.entity.SurveyLinked;
import com.aim.questionnaire.dao.entity.SurveyTemplate;
import org.simplejavamail.api.email.Email;
import org.simplejavamail.api.mailer.Mailer;
import org.simplejavamail.email.EmailBuilder;
import org.simplejavamail.mailer.MailerBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class SurveyService {
    @Autowired
    private SurveyTemplateMapper surveyTemplateMapper;
    @Autowired
    private SurveyLinkedMapper surveyLinkedMapper;

    @Autowired
    private ProjectDataMapper projectDataMapper;

    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    public int addSurvey(SurveyTemplate surveyTemplateEntity) {
        surveyTemplateEntity.setId(UUIDUtil.getOneUUID());
        surveyTemplateMapper.insertSurveyTemplate(surveyTemplateEntity);
        return 0;
    }

    public int linkSurvey(Map<String, Object> map) {
        SurveyTemplate query = SurveyTemplate.queryBuild().id((String) map.get("surveyId")).fetchAll().build();
        SurveyTemplate res = surveyTemplateMapper.querySurveyTemplateLimit1(query);
        String surveyId = UUIDUtil.getOneUUID();
        LocalDateTime startTime = LocalDateTime.parse((String) map.get("startTime"), formatter);
        LocalDateTime endTime = LocalDateTime.parse((String) map.get("endTime"), formatter);
        String projectId = (String) map.get("projectId");
        SurveyLinked newLinkedSurvey = SurveyLinked.bBuild()
                .id(surveyId)
                .surveyName(res.getSurveyName())
                .surveyContent(res.getSurveyContent())
                .surveyStatus(0)
                .startTime(startTime)
                .endTime(endTime)
                .projectId(projectId)
                .build();
        surveyLinkedMapper.insertSurveyLinked(newLinkedSurvey);
        return 0;
    }

    public List<SurveyTemplate> querySurveyListTemplate() {
        List list = new ArrayList();
        SurveyTemplate query = SurveyTemplate.queryBuild().fetchAll().build();
        return surveyTemplateMapper.querySurveyTemplate(query);
    }

    public List<SurveyLinked> querySurveyListLinked(String projectId) {
        List list = new ArrayList();
        SurveyLinked query = SurveyLinked.queryBuild().projectId(projectId).fetchAll().build();
        return surveyLinkedMapper.querySurveyLinked(query);
    }

    public Object querySurveyById(String id) {
        SurveyLinked query = SurveyLinked.queryBuild().id(id).fetchAll().build();
        Object res = surveyLinkedMapper.querySurveyLinkedLimit1(query);
        if (res != null) {
            return res;
        }
        SurveyTemplate query1 = SurveyTemplate.queryBuild().id(id).fetchAll().build();
        return surveyTemplateMapper.querySurveyTemplateLimit1(query1);
    }

    public int modifySurveyTemplate(Map<String, Object> map) {
        String surveyId = (String) map.get("surveyId");
        String surveyName = (String) map.get("surveyName");
        String surveyContent = (String) map.get("surveyContent");
        SurveyTemplate surveyTemplate = SurveyTemplate.bBuild()
                .id(surveyId)
                .surveyName(surveyName)
                .surveyContent(surveyContent)
                .build();
        return surveyTemplateMapper.updateSurveyTemplate(surveyTemplate);
    }

    private void updateProject(int surveyStatus, String projectId) {
        ProjectData query = ProjectData.queryBuild().id(projectId).fetchAll().build();
        ProjectData res = projectDataMapper.queryProjectDataLimit1(query);
        if (surveyStatus != 0) {
            res.setProjectStatus("open");
            projectDataMapper.updateProjectData(res);
        } else {
            SurveyLinked querySurvey = SurveyLinked.queryBuild().projectId(projectId).fetchSurveyStatus().build();
            List<SurveyLinked> list = surveyLinkedMapper.querySurveyLinked(querySurvey);
            for (SurveyLinked item : list) {
                if (item.getSurveyStatus() != 0) {
                    return;
                }
            }
            res.setProjectStatus("closed");
            projectDataMapper.updateProjectData(res);
        }
    }

    public int modifySurveyLinked(Map<String, Object> map) {
        String surveyId = (String) map.get("surveyId");
        String surveyName = (String) map.get("surveyName");
        String surveyContent = (String) map.get("surveyContent");
        String projectId = (String) map.get("projectId");
        LocalDateTime startTime = LocalDateTime.parse((String) map.get("startTime"), formatter);
        LocalDateTime endTime = LocalDateTime.parse((String) map.get("endTime"), formatter);
        Integer surveyStatus = (Integer) map.get("surveyStatus");
        updateProject(surveyStatus, projectId);
        SurveyLinked surveyLinked = SurveyLinked.bBuild()
                .id(surveyId)
                .surveyName(surveyName)
                .surveyContent(surveyContent)
                .projectId(projectId)
                .startTime(startTime)
                .endTime(endTime)
                .surveyStatus(surveyStatus)
                .build();
        return surveyLinkedMapper.updateSurveyLinked(surveyLinked);
    }

    public int deleteSurvey(String id) {
        surveyTemplateMapper.deleteSurveyTemplate(id);
        surveyLinkedMapper.deleteSurveyLinked(id);
        return 0;
    }

    public int sendSurvey(Map<String, Object> map) {
        final String sender = "admin@example.com";

        String content = (String) map.get("context");
        String sendType = (String) map.get("sendType");

        Mailer mailer = MailerBuilder.withSMTPServerHost("localhost").withSMTPServerPort(2525).buildMailer();

        if (sendType.equals("0")) {
            List<Object> list = (List<Object>) map.get("respondentInfo");
            for (Object item : list) {
                Map<String, Object> inner = (Map<String, Object>) item;
                String name = (String) inner.get("name");
                String receiver = (String) inner.get("email");
                Email email = EmailBuilder.startingBlank()
                        .from("admin", sender)
                        .to(name, receiver)
                        .withSubject("survey")
                        .withPlainText(content)
                        .buildEmail();
                mailer.sendMail(email);
            }
        }
        return 0;
    }

}
