package com.aim.questionnaire.service;

import com.aim.questionnaire.common.utils.UUIDUtil;
import com.aim.questionnaire.dao.base.UserDataBaseMapper;
import com.aim.questionnaire.dao.entity.UserData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class UserDataService {

    @Autowired
    private UserDataBaseMapper userDataBaseMapper;

    public Map<String, Object> userLogin(String username, String password) {
        UserData query =
                UserData.queryBuild().fetchAll().account(username).build();
        UserData res = userDataBaseMapper.queryUserDataLimit1(query);
        Map<String, Object> map = new HashMap<String, Object>();
        if (res==null) {
            return map;
        }
        if (!res.getPassword().equals(password)) {
            return map;
        }
        map.put("id",res.getId());
        map.put("role",res.getRole());
        map.put("token", UUIDUtil.getOneUUID());
        return map;
    }

}
