/*
 Navicat Premium Data Transfer

 Source Server         : my
 Source Server Type    : MySQL
 Source Server Version : 50718
 Source Host           : localhost
 Source Database       : questionnaire

 Target Server Type    : MySQL
 Target Server Version : 50718
 File Encoding         : utf-8

 Date: 09/27/2020 22:47:21 PM
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `survey_template`
-- ----------------------------
DROP TABLE IF EXISTS `surveyTemplate`;
CREATE TABLE `surveyTemplate` (
  `id` varchar(50) NOT NULL COMMENT '问卷模板表主键',
  `surveyName` varchar(100) DEFAULT NULL COMMENT '问卷名称',
  `surveyContent` LONGTEXT COMMENT '问卷内容说明',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `survey_template`
-- ----------------------------
BEGIN;
INSERT INTO `surveyTemplate` VALUES ('82904924ee5b466e921c19c71f141391', 'example survey template', '');
COMMIT;

-- ----------------------------
--  Table structure for `surveyLinked`
-- ----------------------------
DROP TABLE IF EXISTS `surveyLinked`;
CREATE TABLE `surveyLinked` (
  `id` varchar(50) NOT NULL COMMENT '问卷模板表主键',
  `surveyName` varchar(100) DEFAULT NULL COMMENT '问卷名称',
  `surveyContent` LONGTEXT COMMENT '问卷内容说明',
  `surveyStatus` BOOL COMMENT '问卷状态',
  `projectId` varchar(50) NOT NULL COMMENT '问卷关联项目id',
  `startTime` datetime COMMENT '开始时间',
  `endTime` datetime COMMENT '结束时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
-- ----------------------------
--  Records of `surveyLinked`
-- ----------------------------
BEGIN;
INSERT INTO `surveyLinked` VALUES ('82904924ee5b466e921c19c71f141301', 'example survey template', '',true,'fdeb8f5312da40b7b89d6b04e7ed8c8f','2020-09-23 20:27:42', '2020-09-23 20:27:59');
COMMIT;

-- ----------------------------
--  Table structure for `userData`
-- ----------------------------
DROP TABLE IF EXISTS `userData`;
CREATE TABLE `userData` (
  `id` varchar(50) NOT NULL COMMENT '用户id主键',
  `account` varchar(100) DEFAULT NULL COMMENT '账户',
  `password` varchar(100) DEFAULT NULL COMMENT '密码',
  `role` varchar(100) DEFAULT NULL COMMENT '用户身份',
  `token` varchar(50) NOT NULL COMMENT '用户token',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
-- ----------------------------
--  Records of `surveyLinked`
-- ----------------------------
BEGIN;
INSERT INTO `userData` VALUES ('028d133ce475431c9ae67ab1d247703b', 'admin', 'admin','admin','fdeb8f5312da40b7b89d6b04e7ed8c8f');
COMMIT;

-- ----------------------------
--  Table structure for `surveyAnswer`
-- ----------------------------
DROP TABLE IF EXISTS `surveyAnswer`;
CREATE TABLE `surveyAnswer` (
  `id` varchar(50) NOT NULL COMMENT '问卷回答表主键',
  `surveyId` varchar(50) DEFAULT NULL COMMENT '问卷id',
  `ip` varchar(50) NOT NULL COMMENT 'ip 地址',
  `startTime` datetime COMMENT '开始时间',
  `endTime` datetime COMMENT '结束时间',
  `answer` LONGTEXT COMMENT '问卷回答',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
-- ----------------------------
--  Records of `surveyLinked`
-- ----------------------------
BEGIN;
INSERT INTO `surveyAnswer` VALUES ('ad6a52d41fae4ccfaba01548e6b8a253', '028d133ce475431c9ae67ab1d247703b', '1.1.1.1','2020-09-23 20:27:42', '2020-09-23 20:27:59','{}');
COMMIT;

-- ----------------------------
--  Table structure for `projectData`
-- ----------------------------
DROP TABLE IF EXISTS `projectData`;
CREATE TABLE `projectData` (
  `id` varchar(50) NOT NULL COMMENT '项目表主键',
  `projectName` varchar(100) DEFAULT NULL COMMENT '项目名称',
  `createdBy` varchar(100) DEFAULT NULL COMMENT '创建者',
  `projectDescription` TEXT COMMENT '项目说明',
  `projectStatus` varchar(100) DEFAULT NULL COMMENT '项目状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
-- ----------------------------
--  Records of `projectData`
-- ----------------------------
BEGIN;
INSERT INTO `projectData` VALUES ('239d8af1b11f43378185d0e63f48b184','test project','028d133ce475431c9ae67ab1d247703b','none','closed');
COMMIT;

-- ----------------------------
--  Table structure for `project_info`
-- ----------------------------
DROP TABLE IF EXISTS `project_info`;
CREATE TABLE `project_info` (
  `id` varchar(50) NOT NULL COMMENT '项目表主键',
  `user_id` varchar(50) DEFAULT NULL COMMENT '用户id（没有用）',
  `project_name` varchar(100) DEFAULT NULL COMMENT '项目名称',
  `project_content` text COMMENT '项目说明',
  `created_by` char(32) DEFAULT NULL COMMENT '创建人',
  `creation_date` datetime DEFAULT NULL COMMENT '创建时间',
  `last_updated_by` char(32) DEFAULT NULL COMMENT '最后修改人',
  `last_update_date` datetime DEFAULT NULL COMMENT '最后修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `project_info`
-- ----------------------------
BEGIN;
INSERT INTO `project_info` VALUES ('283dcf241cf245aea824dc10bbb3d680', '8ceeee2995f3459ba1955f85245dc7a5', '第一个项目', '第一个项目描述 问问', 'admin', '2020-09-23 20:27:42', 'admin', '2020-09-23 20:27:59'), ('4cd6ccb65c894eafaa70b12330f8c2f8', '8ceeee2995f3459ba1955f85245dc7a5', '第一个项目', '第一个项目 的方法v', 'admin', '2020-09-27 16:23:20', 'admin', '2020-09-27 16:23:20');
COMMIT;

-- ----------------------------
--  Table structure for `user_info`
-- ----------------------------
DROP TABLE IF EXISTS `user_info`;
CREATE TABLE `user_info` (
  `id` varchar(50) NOT NULL COMMENT '用户表主键',
  `username` varchar(10) DEFAULT NULL COMMENT '用户名',
  `password` varchar(10) DEFAULT NULL COMMENT '密码',
  `start_time` datetime DEFAULT NULL COMMENT '开始时间',
  `stop_time` datetime DEFAULT NULL COMMENT '截止时间（时间戳）',
  `status` varchar(2) DEFAULT NULL COMMENT '是否启用（1启用，0不启用）',
  `created_by` char(32) DEFAULT NULL COMMENT '创建人',
  `creation_date` datetime DEFAULT NULL COMMENT '创建时间',
  `last_updated_by` char(32) DEFAULT NULL COMMENT '最后修改人',
  `last_update_date` datetime DEFAULT NULL COMMENT '最后修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Records of `user_info`
-- ----------------------------
BEGIN;
INSERT INTO `user_info` VALUES ('8ceeee2995f3459ba1955f85245dc7a5', 'admin', '1', '2018-12-04 21:40:05', '2021-09-27 21:40:00', '1', 'admin', '2018-10-22 09:12:40', 'admin', '2018-12-04 21:40:13');
COMMIT;

-- ----------------------------
--  Procedure structure for `demo_in_parameter`
-- ----------------------------
DROP PROCEDURE IF EXISTS `demo_in_parameter`;
delimiter ;;
CREATE DEFINER=`edu`@`%` PROCEDURE `demo_in_parameter`(IN p_in int)
BEGIN
SELECT p_in;
SET p_in = 2;
SELECT p_in;
END
 ;;
delimiter ;

-- ----------------------------
--  Procedure structure for `sp_name`
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_name`;
delimiter ;;
CREATE DEFINER=`edu`@`%` PROCEDURE `sp_name`(in id int)
begin
SELECT id;
set id = 2;
SELECT id;
end
 ;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
